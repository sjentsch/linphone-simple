#!/bin/bash

set -e -x

PWD=`pwd`
OUTPUT=${PWD}/output

# remove old directories from output
find ${OUTPUT}/* -maxdepth 0 -type d -exec rm -fR {} ";" || true

for ARCH in armhf arm64 amd64; do
   if [ ${ARCH} == "armhf" ]; then
      ARCH2="arm-linux-gnueabihf";
   elif [ "${ARCH}" == "arm64" ]; then
      ARCH2="aarch64-linux-gnu";
   elif [ "${ARCH}" == "amd64" ]; then
      ARCH2="x86_64-linux-gnu";
   fi
   LDFLAGS="-L../output/lib"

   # reset the submodules to remove the compiled files created in previous steps, otherwise CMake-files may still contain the old settings and compilation may fail
   for S in $(grep submodule .gitmodules | cut -d\" -f2); do cd ${S} && git clean -df && cd ..; done

   # check if an old container is still running and terminate it, if necessary
   if docker ps | grep -q linphone; then docker kill linphone; fi
   docker build ./docker/${ARCH} -t bhdouglass/linphone-compile:${ARCH}
   docker run --name linphone -d -v $PWD:$PWD -w $PWD -u `id -u` --rm -i bhdouglass/linphone-compile:${ARCH} bash

   for P in libsrtp speexdsp speex opus bcunit bctoolbox bzrtp ortp belle-sip belr mediastreamer2 linphone; do
      echo "Compiling ${P}"
      docker exec -w ${PWD}/${P} -it linphone bash -c "make distclean || true"
      # run autogen.sh for speexdsp speex and opus
      if [ "${P}" == "speexdsp" ] || [ "${P}" == "speex" ] || [ "${P}" == "opus" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "./autogen.sh"
      fi
      # run configure or CMake with the specific compilation settings for each package
      if [ "${P}" == "libsrtp" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "./configure --prefix=${OUTPUT} --host=${ARCH2} --with-gnu-ld --enable-log-stdout"
         docker exec -w ${PWD}/${P} -it linphone bash -c "make"
         # Don't compile the shared lib, it crashes everything, but for some reason the static is perfectly fine
         #docker exec -w ${PWD}/${P} -it linphone bash -c "make shared_library"
      elif [ "${P}" == "speexdsp" ] && [ "${ARCH}" == "arm"* ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "./configure --prefix=${OUTPUT} --host=${ARCH2} --with-gnu-ld --disable-static --enable-fixed-point --enable-neon --disable-examples"
      elif [ "${P}" == "speexdsp" ] && [ "${ARCH}" != "arm"* ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "./configure --prefix=${OUTPUT} --host=${ARCH2} --with-gnu-ld --disable-static --enable-fixed-point --disable-examples"
      elif [ "${P}" == "speex" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "./configure --prefix=${OUTPUT} --host=${ARCH2} --with-gnu-ld --disable-static --enable-fixed-point --disable-binaries"
      elif [ "${P}" == "opus" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "./configure --prefix=${OUTPUT} --host=${ARCH2} --enable-fixed-point --enable-fuzzing --disable-doc --disable-extra-programs"
      elif [ "${P}" == "bcunit" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT}"
      elif [ "${P}" == "bctoolbox" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT} -DENABLE_MBEDTLS=YES -DENABLE_POLARSSL=NO -DENABLE_DECAF=NO -DENABLE_TESTS_COMPONENT=NO"
      elif [ "${P}" == "bzrtp" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT}"
      elif [ "${P}" == "ortp" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT} -DENABLE_DOC=NO -DENABLE_PERF=YES"  # Enabling PERF makes a huge difference in lag!
      elif [ "${P}" == "belle-sip" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT} -DENABLE_TESTS=NO"
      elif [ "${P}" == "belr" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT} -DENABLE_TOOLS=NO -DENABLE_UNIT_TESTS=NO"
      elif [ "${P}" == "mediastreamer2" ]; then
         docker exec -w ${PWD}/${P} -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT} -DENABLE_VIDEO=NO -DENABLE_TOOLS=NO -DENABLE_UNIT_TESTS=NO -DENABLE_ALSA=NO -DENABLE_SRTP=YES \
                                                                                                   -DENABLE_ZRTP=YES -DENABLE_RELATIVE_PREFIX=YES -DENABLE_MKV=NO -DENABLE_JPEG=NO"
      elif [ "${P}" == "linphone" ]; then
         # move the created libraries to the place where they are expected (output/lib/ARCHITECTURE)
         if [ ! -d ${OUTPUT}/lib/${ARCH2} ]; then mkdir -p ${OUTPUT}/lib/${ARCH2}; fi         
         mv ${OUTPUT}/lib/lib* ${OUTPUT}/lib/${ARCH2}/
         # adjust the search paths in the pkgconfig-files, these are either preceeded with output (string) or ${exec_prefix} (variable)
         sed -i "s/output\/lib/output\/lib\/${ARCH2}/g" output/lib/pkgconfig/*
         sed -i "s/exec_prefix}\/lib/exec_prefix}\/lib\/${ARCH2}/g" output/lib/pkgconfig/*
         # adjust the paths in the cmake-files under output/share
         find output/share/ -name *.cmake -exec sed -i "s/PREFIX}\/lib\/lib/PREFIX}\/lib\/${ARCH2}\/lib/g" {} ";"

         docker exec -w ${PWD}/${P} -e LDFLAGS -it linphone bash -c "cmake . -DCMAKE_INSTALL_PREFIX=${OUTPUT} -DENABLE_GTK_UI=NO -DENABLE_DOC=NO -DENABLE_UNIT_TESTS=NO -DENABLE_NLS=NO -DENABLE_VIDEO=NO \
            -DENABLE_STRICT=NO -DENABLE_VCARD=NO -DENABLE_ASSISTANT=NO -DENABLE_TOOLS=NO -DENABLE_SQLITE_STORAGE=NO -DENABLE_RELATIVE_PREFIX=YES -DENABLE_CXX_WRAPPER=NO -DENABLE_CSHARP_WRAPPER=NO \
            -DCMAKE_PREFIX_PATH=/usr/lib/pkgconfig:/usr/lib/${ARCH2}/pkgconfig:/usr/share/pkgconfig:${OUTPUT}/lib/pkgconfig"
      fi
      docker exec -w ${PWD}/${P} -it linphone bash -c "make && make install"

   done

   echo "Copying extra libs"
   if [ $(ls -1 output/lib/lib* 2> /dev/null | wc -l) -gt 0 ]; then mv ${OUTPUT}/lib/lib* ${OUTPUT}/lib/${ARCH2}/; fi
   docker exec -w ${PWD} -it linphone bash -c "cp /usr/lib/${ARCH2}/libantlr3c-3.2.so* ${OUTPUT}/lib/${ARCH2}"
   docker exec -w ${PWD} -it linphone bash -c "cp /usr/lib/${ARCH2}/libmbedx509.so*    ${OUTPUT}/lib/${ARCH2}"
   docker exec -w ${PWD} -it linphone bash -c "cp /usr/lib/${ARCH2}/libmbedcrypto.so*  ${OUTPUT}/lib/${ARCH2}"

   echo "Cleaning output directoy"
   rm -fR ${OUTPUT}/include
   rm -fR ${OUTPUT}/share/doc
   rm -fR ${OUTPUT}/share/aclocal
   rm -fR ${OUTPUT}/share/BCUnit
   rm -f  ${OUTPUT}/bin/linphone-daemon-pipetest
   find ${OUTPUT} -name *.pdf -delete
   find ${OUTPUT} -name *.a -delete
   find ${OUTPUT} -name *.so -delete
   find ${OUTPUT} -name cmake -exec rm -fR {} +
   find ${OUTPUT} -name pkgconfig -exec rm -fR {} +
   find ${OUTPUT} -type d -empty -delete
   mkdir ${OUTPUT}/bin/${ARCH2}
   mv ${OUTPUT}/bin/linphone* ${OUTPUT}/bin/${ARCH2}/

   echo "Kill Docker-session"0
   docker kill linphone
done

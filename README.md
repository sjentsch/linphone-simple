# SIP Client for Ubuntu Touch

This project is a SIP client for Ubuntu Touch using linphone as a base.

In order to run linphone on an Ubuntu Touch device it needs to be specifically
compiled for them. This is due to the codecs needing specific compiler directives
for the arm platform (the debs in apt work, but not well enough to make a call).
Additionally ortp has a small tweak to fix some issues with click packages and
their confinement. Linphone also has some tweaks to the build process and some
segfault fixes.

## Developing

- Install [clickable](https://github.com/bhdouglass/clickable)
- In the root of the repo, run `clickable`
- The app will be compiled and installed on your connected device

## Building

Building is can be done on a desktop with [clickable](https://github.com/bhdouglass/clickable)
setup and working.

- Clone this repo: `git clone https://gitlab.com/ubports-linphone/compile-linphone-ubuntu-touch.git`
- Initialize submodules: `git submodule update --init --recursive`
- If you build in the same directory before, it is recommended that you reset the submodules: `for S in $(grep submodule .gitmodules | cut -d\" -f2); do cd ${S} && git reset --hard && cd ..; done`
- Compile linphone and dependencies: `./build.sh`

## Testing

- Copy binaries and libraries to your phone: `adb push ./output /home/phablet/linphone/`
- Connect to the phone: `clickable shell`
- Run linphonec: `LD_LIBRARY_PATH=/home/phablet/linphone/lib/arm-linux-gnueabihf linphone/bin/linphonec`

## Linphone

Prebuild binaries and libraries for running linphone on Ubuntu Touch can be
found in `/linphone`. These have been compiled using scripts found on Gitlab:
<https://gitlab.com/ubports-linphone/compile-linphone-ubuntu-touch>

## Translating

Translations must be done [here](https://webtranslate.org/projects/linphone/)

## Acknowledgments

- Malte for setting up the weblate service

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
